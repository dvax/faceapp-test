package com.test.photoviewer.data

import androidx.room.*

@Entity(tableName = "photos")
data class PhotoEntity(
    @PrimaryKey val id: String,
    @ColumnInfo(name = "url") val url: String
)

@Dao
interface PhotoDao {
    @Query("SELECT * FROM photos")
    fun getAll(): List<PhotoEntity>

    @Insert
    fun insertAll(vararg photoEntities: PhotoEntity)

    @Query("DELETE FROM photos")
    fun truncateTable()
}

@Database(entities = [PhotoEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun photoDao(): PhotoDao
}