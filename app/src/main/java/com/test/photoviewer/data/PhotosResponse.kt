package com.test.photoviewer.data

data class PhotosResponse(val photos: PhotoResponseInfo)

data class PhotoResponseInfo(
    val photo: List<Photo>
)

data class Photo(
    val id: String,
    val farm: String,
    val server: String,
    val secret: String,
) {
    fun getUrl(): String {
        return "https://farm${farm}.staticflickr.com/${server}/${id}_${secret}.jpg"
    }
}