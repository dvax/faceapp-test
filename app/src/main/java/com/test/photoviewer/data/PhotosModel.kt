package com.test.photoviewer.data

import android.content.Context
import com.test.photoviewer.Api
import com.test.photoviewer.hasNetwork
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class PhotosModel @Inject constructor(
    private val api: Api,
    private val dao: PhotoDao,
    private val context: Context
) {

    fun getPhotos(): Single<List<PhotoEntity>> {
        return if (hasNetwork(context)) {
            api.getList()
                .map { resp -> resp.photos.photo.map { PhotoEntity(it.id, it.getUrl()) } }
                .doOnSuccess {
                    dao.truncateTable()
                    dao.insertAll(*it.toTypedArray())
                }
        } else {
            Single.fromCallable { dao.getAll() }
        }
    }

}