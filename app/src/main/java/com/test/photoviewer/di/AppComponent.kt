package com.test.photoviewer.di

import android.content.Context
import com.test.photoviewer.MainViewModel
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [NetworkModule::class, StoreModule::class])
interface AppComponent {

    fun inject(viewModel: MainViewModel)

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance context: Context): AppComponent
    }
}