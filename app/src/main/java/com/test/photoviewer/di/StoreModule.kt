package com.test.photoviewer.di

import android.content.Context
import androidx.room.Room
import com.test.photoviewer.data.AppDatabase
import com.test.photoviewer.data.PhotoDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class StoreModule {

    @Provides
    @Singleton
    fun provideDB(context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, "database").build()
    }

    @Provides
    @Singleton
    fun provideDao(db: AppDatabase): PhotoDao {
        return db.photoDao()
    }
}