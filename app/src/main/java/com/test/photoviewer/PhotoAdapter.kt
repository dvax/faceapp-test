package com.test.photoviewer

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.test.photoviewer.data.Photo
import com.test.photoviewer.data.PhotoEntity

class PhotoAdapter(private val imgClick: (String) -> Unit) :
    RecyclerView.Adapter<PhotoAdapter.ViewHolder>() {

    private val items: MutableList<PhotoEntity> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_photo, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun setItems(newItems: List<PhotoEntity>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val preview: ImageView = itemView.findViewById(R.id.preview)

        fun bind(photo: PhotoEntity) {
            Glide.with(itemView)
                .load(photo.url)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(preview)

            preview.setOnClickListener {
                imgClick.invoke(photo.url)
            }
        }

    }

}