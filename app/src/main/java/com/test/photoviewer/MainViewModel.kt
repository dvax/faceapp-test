package com.test.photoviewer

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.test.photoviewer.data.PhotoEntity
import com.test.photoviewer.data.PhotosModel
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class MainViewModel(private val state: SavedStateHandle) : ViewModel() {

    @Inject
    lateinit var photosModel: PhotosModel

    private val photoLiveData: MutableLiveData<List<PhotoEntity>> = MutableLiveData()

    private val errorLiveData: MutableLiveData<String> = SingleLiveEvent()

    fun getPhotos(): LiveData<List<PhotoEntity>> {
        return photoLiveData
    }

    fun getErrors(): LiveData<String> {
        return errorLiveData
    }

    fun loadPhotos() {
        state.set(LOADING_STATUS, LOADING)
        photosModel.getPhotos()
            .subscribeOn(Schedulers.io())
            .subscribe({
                photoLiveData.postValue(it.take(PHOTO_COUNT))
                state.set(LOADING_STATUS, LOADED)
            }, {
                state.remove<Int>(LOADING_STATUS)
                errorLiveData.postValue(it.localizedMessage)
            })
    }

    fun loadPhotosIfNeed() {
        if (!(state.contains(LOADING_STATUS) && state.get<Int>(LOADING_STATUS) == LOADED)) {
            loadPhotos()
        }
    }

    companion object {
        private const val PHOTO_COUNT = 20

        private const val LOADING = 1
        private const val LOADED = 2

        private const val LOADING_STATUS = "LOADING_STATUS"
    }
}