package com.test.photoviewer

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.test.photoviewer.FullscreenActivity.Companion.ARG_URL
import com.test.photoviewer.PhotoApp.Companion.getAppComponent

class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModels()

    private val adapter = PhotoAdapter(this::openFullScreen)

    private lateinit var swipeRefreshLayout: SwipeRefreshLayout

    private lateinit var emptyState: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getAppComponent(this).inject(viewModel)

        emptyState = findViewById(R.id.empty_state)
        swipeRefreshLayout = findViewById(R.id.refresh_layout)

        val buttonRefresh: Button = findViewById(R.id.try_again_button)
        val list: RecyclerView = findViewById(R.id.list)

        list.adapter = adapter
        val percent = resources.getInteger(R.integer.percent)
        val spanCount = resources.getInteger(R.integer.span)
        val margin = resources.getDimensionPixelSize(R.dimen.default_margin)
        list.addItemDecoration(GridSpacingItemDecoration(spanCount, percent, margin))

        swipeRefreshLayout.isRefreshing = true

        viewModel.getErrors().observe(this, {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
            swipeRefreshLayout.isRefreshing = false
            if (adapter.itemCount == 0) {
                emptyState.visibility = View.VISIBLE
            }
        })
        viewModel.getPhotos().observe(this, {
            swipeRefreshLayout.isRefreshing = false
            adapter.setItems(it)
            swipeRefreshLayout.visibility = View.VISIBLE
            if (it.isNotEmpty()) {
                emptyState.visibility = View.GONE
            } else {
                emptyState.visibility = View.VISIBLE
            }
        })

        swipeRefreshLayout.setOnRefreshListener {
            loadPhoto()
        }

        buttonRefresh.setOnClickListener {
            loadPhoto()
        }

        viewModel.loadPhotosIfNeed()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.refresh) {
            loadPhoto()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun loadPhoto() {
        swipeRefreshLayout.isRefreshing = true
        viewModel.loadPhotos()
    }

    private fun openFullScreen(url: String) {
        val fullScreenIntent = Intent(this, FullscreenActivity::class.java).apply {
            putExtra(ARG_URL, url)
        }
        startActivity(fullScreenIntent)
    }
}