package com.test.photoviewer

import com.test.photoviewer.data.PhotosResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET

interface Api {
    @GET("?method=flickr.photos.getRecent&api_key=da9d38d3dee82ec8dda8bb0763bf5d9c&format=json&nojsoncallback=1")
    fun getList(): Single<PhotosResponse>
}