package com.test.photoviewer

import android.app.Application
import android.content.Context
import com.test.photoviewer.di.DaggerAppComponent

class PhotoApp : Application() {

    val appComponent = DaggerAppComponent.factory().create(this)

    companion object {
        private fun getApplication(context: Context) = context.applicationContext as PhotoApp
        fun getAppComponent(context: Context) = getApplication(context).appComponent
    }
}