package com.test.photoviewer

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class GridSpacingItemDecoration(
    private val spanCount: Int,
    private val sizeInPercent: Int,
    private val topBottomMargin: Int
) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val position = parent.getChildAdapterPosition(view)
        val column = position % spanCount
        val spacing =
            ((parent.width - parent.width * (sizeInPercent / 100F) * spanCount) / (spanCount + 1)).toInt()
        outRect.left = spacing - column * spacing / spanCount
        outRect.right = (column + 1) * spacing / spanCount
        if (position < spanCount) {
            outRect.top = topBottomMargin
        }
        outRect.bottom = topBottomMargin

    }

}
